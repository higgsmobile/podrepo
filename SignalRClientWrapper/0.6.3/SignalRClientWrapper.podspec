Pod::Spec.new do |s|
  s.name             = 'SignalRClientWrapper'
  s.version          = '0.6.3'
  s.summary          = 'Wrapper for SwiftSignalRClient.'
  s.homepage         = 'https://gitlab.com/higgsmobile/zeta'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = 'app'
  s.source           = { :http => 'https://gitlab.com/api/v4/projects/55324491/packages/generic/XCFramework_SignalRClientWrapper/0.6.3/SignalRClientWrapper.xcframework.zip',
                         :headers => ['PRIVATE-TOKEN: glpat-QJauZJBvT8cvWJb783wm'] }
  s.ios.deployment_target = '13.0'
  s.swift_versions = [5.0]
  s.vendored_frameworks = 'SignalRClientWrapper.xcframework'
  s.dependency 'SwiftSignalRClient'
  s.dependency 'AnyCodable-FlightSchool'
end
