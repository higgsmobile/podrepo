Pod::Spec.new do |spec|
    spec.name                     = 'shared'
    spec.version                  = '1.0.6'
    spec.homepage                 = 'Link to the Shared Module homepage'
    spec.source                   = { :http => 'https://gitlab.com/api/v4/projects/55324491/packages/generic/XCFramework/1.0.6/shared.xcframework.zip',
                                      :headers => ['PRIVATE-TOKEN: glpat-QJauZJBvT8cvWJb783wm'] }
    spec.authors                  = 'app'
    spec.license                  = '{:type => LGPL}'
    spec.summary                  = 'Zeta SDK'
    spec.vendored_frameworks      = 'shared.xcframework'
    spec.libraries                = 'c++'
    spec.ios.deployment_target = '16.0'
    spec.dependency 'SignalRClientWrapper', '0.3.0'
end