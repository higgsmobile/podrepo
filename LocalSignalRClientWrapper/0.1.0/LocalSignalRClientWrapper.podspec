Pod::Spec.new do |s|
  s.name             = 'LocalSignalRClientWrapper'
  s.version          = '0.1.0'
  s.summary          = 'Wrapper for SwiftSignalRClient.'
  s.homepage         = 'https://gitlab.com/higgsmobile/zeta'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = 'app'
  s.source           = { :git => 'git@gitlab.higgstar.com:mobile/signalrclientwrapper.git', :tag => s.version.to_s }
  s.ios.deployment_target = '13.0'
  s.swift_versions = [5.0]
  s.source_files = 'SignalRClientWrapper/Classes/**/*'
  s.dependency 'SwiftSignalRClient'
  s.dependency 'AnyCodable-FlightSchool'
end
