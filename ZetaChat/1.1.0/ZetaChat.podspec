Pod::Spec.new do |spec|
    spec.name                     = 'ZetaChat'
    spec.version                  = '1.1.0'
    spec.homepage                 = 'Link to the Shared Module homepage'
    spec.source                   = { :http => 'https://gitlab.com/api/v4/projects/55324491/packages/generic/XCFramework_ZetaChat/1.1.0/ZetaChat.xcframework.zip',
                                      :headers => ['PRIVATE-TOKEN: glpat-QJauZJBvT8cvWJb783wm'] }
    spec.authors                  = 'app'
    spec.license                  = '{:type => LGPL}'
    spec.summary                  = 'Zeta SDK'
    spec.vendored_frameworks      = 'ZetaChat.xcframework'
    spec.libraries                = 'c++'
    spec.ios.deployment_target = '16.0'
    spec.dependency 'SignalRClientWrapper'
end