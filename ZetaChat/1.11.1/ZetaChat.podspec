Pod::Spec.new do |spec|
    spec.name                     = 'ZetaChat'
    spec.swift_version            = '5.0'
    spec.version                  = '1.11.1'
    spec.homepage                 = 'https://gitlab.com/higgsmobile/zetademo'
    spec.source                   = { :http => 'https://gitlab.com/api/v4/projects/66022484/packages/generic/XCFramework_ZetaChat/1.11.1/ZetaChat.xcframework.zip',
                                      :headers => ['PRIVATE-TOKEN: glpat-y3xzzZLvLxy3dxTVA9A7'] }
    spec.authors                  = 'app'
    spec.license                  = { :type => 'Apache License, Version 2.0', :text => <<-LICENSE
                                     Licensed under the Apache License, Version 2.0 (the "License");
                                     you may not use this file except in compliance wiAth the License.
                                     You may obtain a copy of the License at

                                     http://www.apache.org/licenses/LICENSE-2.0

                                     Unless required by applicable law or agreed to in writing, software
                                     distributed under the License is distributed on an "AS IS" BASIS,
                                     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
                                     See the License for the specific language governing permissions and
                                     limitations under the License.
                                     LICENSE
                                   }
    spec.summary                  = 'Zeta SDK'
    spec.vendored_frameworks      = 'ZetaChat.xcframework'
    spec.libraries                = 'c++'
    spec.ios.deployment_target = '15.0'
end
